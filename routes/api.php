<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\tranController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', 'Api\RegisterController@login');
Route::post('register', 'Api\RegisterController@register');

Route::middleware('auth:api')->group(function () {
    Route::resource('Banner',Api\BannerController::class);
    Route::resource('transaction',tranController::class);
    // Route::put('update_result/{id}',[tranController::class,'update_result'])->name('update_result');
    Route::get('user_tran/{id}',[tranController::class,'user_tran'])->name('user_tran');
    Route::get('price',[tranController::class,'price'])->name('price');
});