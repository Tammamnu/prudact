<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $price = \App\Models\price::count();
        // if($price==0)
        // \App\Models\price::create([
        //     'name' => 'اعتراض عملي',
        //     'price' => '5000',
        // ]);

        // $price = \App\Models\price::count();
        // if($price==0)
        // App\Models\price::create([
        //     'name' => 'وثيقه دوام',
        //     'price' => '5000'
        // ]);

        // $price = \App\Models\price::count();
        // if($price==0)
        // App\Models\price::create([
        //     'name' => 'كشف علامات',
        //     'price' => '5000'
        // ]);

        $settings = \App\Models\price::count();
        if($settings==0)
        DB::table('prices')->insert([
            'name' => Str::random(0).'اعتراض عملي',
            'price' => Str::random(0).'5000',
        ]);

        $settings = \App\Models\price::count();
        if($settings==1)
        DB::table('prices')->insert([
            'name' => Str::random(0).'وثيقه دوام',
            'price' => Str::random(0).'5000',
        ]);

        $settings = \App\Models\price::count();
        if($settings==2)
        DB::table('prices')->insert([
            'name' => Str::random(0).'كشف علامات',
            'price' => Str::random(0).'5000',
        ]);


    }
    
}
