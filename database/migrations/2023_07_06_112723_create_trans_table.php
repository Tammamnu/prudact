<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');

            $table->unsignedBigInteger('auther_id')->nullable();
            // $table->unsignedBigInteger('price_id');

            $table->text('tran_name')->nullable();
            $table->text('father')->nullable();
            $table->text('mother')->nullable();
            $table->text('section')->nullable();
            $table->text('reg_uni')->nullable();//سنه التسجيل في الجامعه
            $table->text('year')->nullable();
            $table->text('course_name')->nullable();//اسم المقرر
            $table->text('course_year')->nullable();
            $table->text('chapter')->nullable();
            $table->text('teach')->nullable();
            $table->text('price')->nullable();
            $table->text('state')->default('انتظار');
            $table->timestamps();
            $table->foreign('auther_id')->references('id')->on("users")->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on("users")->onDelete('cascade');
            // $table->foreign('price_id')->references('id')->on("prices")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans');
    }
};
