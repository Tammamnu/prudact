<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tran;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class searchController extends Controller
{
public function search(Request $request , $id){

  $tran = tran::find($id);
  
 if($request->state or $request->name or $request->id_uni !== null){     

    if($request->name){
        
    $tran = User::where('name', 'LIKE' ,'%'.$request->name.'%')->latest()->paginate(10);
      return view('admin.tran.all' ,compact('tran' ));
      }

      if($request->state){
        
        $tran = tran::where('state', 'LIKE' ,'%'.$request->state.'%')->latest()->paginate(10);
        return view('admin.tran.all' ,compact('tran'));
          }
          
          if($request->id_uni){
        
             
            $tran = tran::where('id_uni', 'LIKE' ,'%'.$request->id_uni.'%')->latest()->paginate(10);
            // $tran = User::join('trans' , 'trans.user_id' , '=' , 'users.id')->where('trans.id' , 'id')->where('users.id_uni','LIKE' ,'%'.$request->id_uni.'%')->latest()->paginate(10);
            return view('admin.tran.all' , compact('tran'));
              }

          
    }else{
        return redirect()->back();
    }

}
}
?>