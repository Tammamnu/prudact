<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Resources\tran as tranResource;
use App\Http\Resources\price as priceResource;
use App\Models\auther;
use App\Models\tran;
use App\Models\price;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;

class tranController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp = auther::all();
        $user = User::all();
        $tran = tran::all();
        return $this->sendResponse(tranResource::collection($tran , $user , $emp) , 'this is tran');
    }


    public function store(Request $request)
    {
        $input = $request->all();
        $Validator = Validator::make($input , 
        [
            // 'user_id' => 'required',
            'tran_name' => 'required',
            // 'father' => 'required',
            // 'mother' => 'required',
            // 'section' => 'required',
            // 'reg_uni' => 'required',
            // 'year' => 'required',
            // 'course_name' => 'required',
            // 'course_year' => 'required',
            // 'chapter' => 'required',
            // 'teach' => 'required',
            // 'state' => 'required',
        ]);

        if ($Validator->fails()) {
            return $this->sendError('plase validate error' , $Validator->errors());
        }
        $user=Auth::user();
        $input['user_id'] = $user->id;
        $tran= tran::create($input);
        return $this->sendResponse(new tranResource($tran) , 'transaction created  successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tran = tran::find($id);
        if(is_null($tran)){
            return $this->sendError('transaction not found!' );
        }
        return $this->sendResponse(new tranResource($tran) , 'transaction retireved  successfully');
    }

    public function user_tran($id)
    {

        $tran = tran::where('user_id' , $id)->get();
        if(is_null($tran)){
            return $this->sendError('transaction not found!' );
        }
        return $this->sendResponse(tranResource::collection($tran) , 'this is transaction for you');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $input = $request->all();
        // $Validator = Validator::make($input , 
        // [

        // ]);

        // if ($Validator->fails()) {
        //     return $this->sendError('plase validate error' , $Validator->errors());
        // }
        $tran = tran::find($id);

        
        if($tran->user_id != Auth::id() ){
            return $this->sendError('you dont have rights');
        }
            $tran->tran_name = $input['tran_name'];
            $tran->father = $input['father'];
            $tran->mother = $input['mother'];
            $tran->section = $input['section'];
            $tran->reg_uni = $input['reg_uni'];
            $tran->year = $input['year'];
            $tran->course_name = $input['course_name'];
            $tran->course_year = $input['course_year'];
            $tran->chapter = $input['chapter'];
            $tran->teach = $input['teach'];


        $tran->save();

        return $this->sendResponse(new tranResource($tran) , 'transaction update  successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $errorMasseage =[];
        $tran = tran::find($id);
        if($tran->state !== 'انتظار' ){
        if($tran->user_id != Auth::id() ){
            return $this->sendError('you dont have rights' , $errorMasseage);
        }
        
            return $this->sendError('لا يمكنك الحذف عندما تكون المعامله قيد العمل');
        }
        $tran->delete();
        return $this->sendResponse(new tranResource($tran), 'User deleted  successfully');
    }

    public function price( )
    {
 
        $price = price::all();
        return $this->sendResponse(priceResource::collection($price ) , 'this is price');
    }


    // public function update_result(Request $request,  $id)
    // {
    //     $input = $request->all();
    //     $tran = tran::find($id);

    //     $tran->father = $input['father'];
    //     $tran->mother = $input['mother'];
    //     $tran->section = $input['section'];
    //     $tran->reg_uni = $input['reg_uni'];

    //     $tran->save();
    //     if($tran->user_id != Auth::id() ){
    //         return $this->sendError('you dont have rights');
    //     }

    //     return $this->sendResponse(new tranResource($tran) , 'transaction update  successfully');
    // }

}
