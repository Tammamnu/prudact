<?php

namespace App\Http\Controllers;

use App\Models\tran;
use App\Models\User;
use App\Models\price;
use App\Models\auther;
use App\Mail\MyTestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TranController extends Controller
{

    public function __construct()
    {
        $this->middleware('CheckRole:ADMIN|EDITOR');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price = price::all();
        if (auth()->user()->power == "ADMIN")
        {
        $tran = tran::all();
        $auther_id = tran::select("auther_id");
        return view('admin.tran.all', compact('tran') , compact('auther_id' , 'price') );
        }else{
            $tran = tran::where('auther_id' ,  null)->get();
            return view('admin.tran.all', compact('tran') , compact('price') );
        }
    }

    public function my_tran( $id)
    {
        $auther_id = tran::where('auther_id' , '=' , $id)->get();
        return view('admin.tran.my_tran', compact('auther_id')  );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tran  $tran
     * @return \Illuminate\Http\Response
     */
    public function show(tran $tran)
    {
        return view('admin\tran\all');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tran  $tran
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $auther = Auth::user();
        // $auther = User::select("*")->where('id' , '=' , $name->id);
        $tran = tran::find($id);
        $emp = user::where('power' ,'EDITOR')->orwhere('power' , 'ADMIN')->get();
        return view('admin.tran.edit', compact('tran' , 'auther' , 'emp' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tran  $tran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tran $tran , $id)
    {

        if(!auth()->user()->has_access_to('update',$tran))abort(403);
        $request->validate([
            // 'title'=>"required",
        ]);

        // $user = User::find($id);
        $tran = tran::find($id);
        $tran->update ([

            'tran_name'=> $request->tran_name,
            'father'=> $request->father,
            'state'=> $request->state,
            'mother'=> $request->mother,
            'section'=> $request->section,
            'reg_uni'=> $request->reg_uni,
            'year'=> $request->year,
            'course_name'=> $request->course_name,
            'course_year'=> $request->course_year,
            'chapter'=> $request->chapter,
            'teach'=> $request->teach,
            'auther_id'=> $request->auther_id,


            // 'content'=> $request->content,
        ]);

        $tran->save();

       
        
        if ( $request->state == 'عمليه الدفع') {
            $data = tran::all();
            $user = user::select('email')->where( 'id'  , $tran->user_id)->get();
            Mail::to($user)->send(new MyTestMail($data->toArray()));
        }
       
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tran  $tran
     * @return \Illuminate\Http\Response
     */
    public function destroy(tran $tran , $id)
    {
        $tran = tran::find($id);
        if($tran->state !== 'انتظار' ){
            flash()->success('لا يمكنك الحذف عندما تكون المعامله قيد العمل');
        }else{ 
        $tran->delete();
    }
    return redirect()->back();
    }
    /**
     * Remove the specified resource from storage.
     *
     */
    public function pay()
    {
            $pay = tran::where('state' ,  'عمليه الدفع')->get();
            return view('admin.tran.pay', compact('pay') );
    }

    public function changeStatus(Request $request)
    {
        $user = User::find($request->id);
        $user->approve = $request->approve;
        $user->save();
  
        return response()->json(['success'=>'تم تغيير الحاله بنجاح']);
    }
}
