<?php

namespace App\Http\Resources;

use App\Models\tran as trans;
use Illuminate\Http\Resources\Json\JsonResource;

class tran extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        

        return [
            'id' => $this->id ,
            'user_name' => $this->user->name,
            'user_id' => $this->user_id,
            'auther_id' => $this->auther?->name,
            'tran_name' => $this->tran_name,
            'father' => $this->father,
            'mother' => $this->mother,
            'section' => $this->section,
            'reg_uni' => $this->reg_uni,
            'year' => $this->year,
            'course_name' => $this->course_name,
            'course_year' => $this->course_year,
            'chapter' => $this->chapter,
            'teach' => $this->teach,
            'state' => $this->state,
        ];
}
}
