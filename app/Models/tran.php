<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Summary of tran
 */
class tran extends Model
{
    public $guarded=['id','created_at','updated_at'];


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function auther(): BelongsTo
    {
        return $this->belongsTo(User::class,'auther_id');
    }
}
