@extends('layouts.admin')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"  />
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@section('content')
<div class="col-12 p-3">
	<div class="col-12 col-lg-12 p-0 main-box">
	 
		<div class="col-12 px-0">
			<div class="col-12 p-0 row">
				<div class="col-12 col-lg-4 py-3 px-3">
					<span class="fas fa-users"></span>	المستخدمين
				</div>
				<div class="col-12 col-lg-4 p-2">
				</div>
				<div class="col-12 col-lg-4 p-2 text-lg-end">
					<a href="{{route('admin.users.create')}}">
					<span class="btn btn-primary"><span class="fas fa-plus"></span> إضافة جديد</span>
					</a>
				</div>
			</div>
			<div class="col-12 divider" style="min-height: 2px;"></div>
		</div>

		<div class="col-12 py-2 px-2 row">
			<div class="col-12 col-lg-4 p-2">
				<form method="GET">
					<input type="text" name="q" class="form-control" placeholder="بحث ... ">
				</form>
			</div>
		</div>
		<div class="col-12 p-3" style="overflow:auto">
			<div class="col-12 p-0" style="min-width:1100px;">
				
			
			<table class="table table-bordered  table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>الاسم</th>
						<th>البريد</th>
						<th>تحكم</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{$user->id}}</td>
						<td>{{$user->name}}</td>
						<td>{{$user->email}}</td>
						<td>
							@if(auth()->user()->has_access_to('update',$user))
							<a href="{{route('admin.users.edit',$user)}}">
							<span class="btn  btn-outline-success btn-sm font-1 mx-1">
								<span class="fas fa-wrench "></span> تحكم
							</span>
							</a>
							@endif
							@if(auth()->user()->has_access_to('delete',$user))
							<form method="POST" action="{{route('admin.users.destroy',$user)}}" class="d-inline-block">@csrf @method("DELETE")
								<button class="btn  btn-outline-danger btn-sm font-1 mx-1" onclick="var result = confirm('هل أنت متأكد من عملية الحذف ؟');if(result){}else{event.preventDefault()}">
									<span class="fas fa-trash "></span> حذف
								</button>
							</form>

							
							<input data-id="{{$user->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $user->approve ? 'checked' : '' }}>
							
							{{-- @if($user->approve=="0")  
							<form method="post" action="{{route('admin.users.approve',$user)}}" class="d-inline-block">
								@csrf 
								@method("PUT")
								<button  class="btn  btn-outline-success btn-sm font-1 mx-1">
									<span  value="1"  class="fas fa-true "></span> موافقه
								</button>
								@else
								<button class="btn  btn-outline-danger btn-sm font-1 mx-1" >
									<span value="2" class="fas fa-trash "></span> حذف
								</button>
							</form> --}}
							{{-- @if($user->approve=="0")  
							<a  href="{{route('admin.users.approve',$user)}}">
								<span class="btn  btn-outline-success btn-sm font-1 mx-1">
									<button type="submit"  value="1" ></button> موافقه
								</span>
								@else
								<span class="btn  btn-outline-success btn-sm font-1 mx-1">
									<button type="submit" value="1" ></button> حذف
								</span>
								</a> --}}
							{{-- @endif --}}
							{{-- <div class="col-12 p-2">
								<div class="col-12">
									محظور
								</div>
								<div class="col-12 pt-3">
									<select class="form-control" name="blocked">
										<option @if($user->blocked=="0") selected @endif value="0">لا</option>
										<option @if($user->blocked=="1") selected @endif value="1">نعم</option>
									</select>
								</div>
							</div> --}}
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-12 p-3">
			{{$users->appends(request()->query())->render()}}
		</div>
	</div>
</div>

<script>

	$(function() {
  
	  $('.toggle-class').change(function() {
  
		  var approve = $(this).prop('checked') == true ? 1 : 0; 
  
		  var user_id = $(this).data('id'); 
  
		   
  
		  $.ajax({
  
			  type: "GET",
  
			  dataType: "json",
  
			  url: '/changeStatus',
  
			  data: {'status': status, 'user_id': user_id},
  
			  success: function(data){
  
				console.log(data.success)
  
			  }
  
		  });
  
	  })
  
	})
  
  </script>
@endsection
