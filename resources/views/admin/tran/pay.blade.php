@extends('layouts.admin')
@section('content')
<div class="col-12 p-3">
	<div class="col-12 col-lg-12 p-0 main-box">
	 
		<div class="col-12 px-0">
			<div class="col-12 p-0 row">
				<div class="col-12 col-lg-4 py-3 px-3">
					<span class="fas fa-articles"></span> المعاملات
				</div>
				<div class="col-12 col-lg-4 p-2">
				</div>
				<div class="col-12 col-lg-4 p-2 text-lg-end">

				</div>
			</div>
			<div class="col-12 divider" style="min-height: 2px;"></div>
		</div>

		<div class="col-12 py-2 px-2 row">
			<div class="col-12 col-lg-4 p-2">
				<form action="{{route('search' , auth()->user()->id)}}" method="GET">
					<input type="text" name="id_uni" class="form-control" placeholder="بحث ... ">

			</div>
	
		</div>
		
		<div class="col-12 p-3" style="overflow:auto">
			<div class="col-12 p-0" style="min-width:1100px;">
				
			
			<table class="table table-bordered  table-hover">


				<thead>
					<tr>
						<th>#</th>
                        <th>اسم الطالب</th>
						<th>الرقم الجامعي</th>
						<th>المعامله</th>
						<th>السنه الحاليه</th>
                        <th >تاريخ الطلب</th>
						<th>تحكم</th>
					</tr>
				</thead>
				<tbody>
					@foreach($pay as $tran)
					<tr>
						<td>{{$tran->id}}</td>
						<td>{{$tran->user->name}}</td>
                        <td>{{$tran->user->id_uni}}</td>
						<td>{{$tran->tran_name}}</td>
                        <td>{{$tran->year}}</td>
						<td>{{$tran->created_at}}</td>

						<td style="width: 270px;">

							@if(auth()->user()->has_access_to('update',$tran))
							<a href="{{route('admin.transaction.edit',$tran)}}">
								<span class="btn  btn-outline-success btn-sm font-1 mx-1">
									<span class="fas fa-wrench "></span> تحكم
								</span>
							</a>
							@endif
							@if(auth()->user()->has_access_to('delete',$tran))
							<form method="POST" action="{{route('admin.transaction.destroy',$tran)}}" class="d-inline-block">@csrf @method("DELETE")
								<button class="btn  btn-outline-danger btn-sm font-1 mx-1" onclick="var result = confirm('هل أنت متأكد من عملية الحذف ؟');if(result){}else{event.preventDefault()}">
									<span class="fas fa-trash "></span> حذف
								</button>
							</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-12 p-3">
			
		</div>
	</div>
</div>
@endsection
