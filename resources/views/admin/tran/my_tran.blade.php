@extends('layouts.admin')
@section('content')
<div class="col-12 p-3">
	<div class="col-12 col-lg-12 p-0 main-box">
	 
		<div class="col-12 px-0">
			<div class="col-12 p-0 row">
				<div class="col-12 col-lg-4 py-3 px-3">
					<span class="fas fa-articles"></span> المعاملات
				</div>
				<div class="col-12 col-lg-4 p-2">
				</div>
				<div class="col-12 col-lg-4 p-2 text-lg-end">

				</div>
			</div>
			<div class="col-12 divider" style="min-height: 2px;"></div>
		</div>

		<div class="col-12 py-2 px-2 row">
			<div class="col-12 col-lg-4 p-2">
				<form action="{{route('search' , auth()->user()->id)}}" method="GET">
					<input type="text" name="id_uni" class="form-control" placeholder="بحث ... ">

			</div>
	
		</div>
		<div  class="col-12 py-2 px-2 row"> 

				<div class="col-12 col-lg-4 p-2">
					<select class="form-control" name="state" >
						<option value selected disabled hidden> الفرز حسب الحاله</option>

						<option  value=" انتظار" > انتظار</option>
						<option  value="مقبوله و قيد العمل" >مقبوله و قيد العمل</option>
						<option  value="عمليه الدفع">عمليه الدفع</option>
						<option  value="تم الانجاز" >تم الانجاز</option>
						<option  value="مرفوضه" >مرفوضه</option>

					</select>
				</div>
				<div class="col-12 p-3">
					<button class="btn btn-success" id="submitEvaluation">فرز</button>
				</div>
			</form>
		</div>
		
		<div class="col-12 p-3" style="overflow:auto">
			<div class="col-12 p-0" style="min-width:1100px;">
				
			
			<table class="table table-bordered  table-hover">


				<thead>
					<tr>
						<th>#</th>
						<th>المعامله</th>
						<th>الرقم الجامعي</th>
                        <th>اسم الطالب</th>
						<th>السنه الحاليه</th>
                        <th >الحاله</th>
						<th>تحكم</th>
					</tr>
				</thead>
				<tbody>
					@foreach($auther_id as $tran)
					<tr>
						<td>{{$tran->id}}</td>
						<td>{{$tran->tran_name}}</td>
                        <td>{{$tran->user->id_uni}}</td>
						<td>{{$tran->user->name}}</td>
                        <td>{{$tran->year}}</td>
                       @if($tran->state == 'انتظار') <td style="background-color: rgba(218, 165, 32, 0.623)">{{$tran->state}}</td>@endif
					   @if($tran->state == 'مقبوله و قيد العمل') <td style="background-color: rgba(31, 86, 206, 0.459)">{{$tran->state}}</td>@endif
					   @if($tran->state == 'تم الانجاز') <td style="background-color: rgba(45, 180, 86, 0.774)">{{$tran->state}}</td>@endif
					   @if($tran->state == 'عمليه الدفع') <td style="background-color: rgba(62, 65, 63, 0.774)">{{$tran->state}}</td>@endif
					   @if($tran->state == 'مرفوضه') <td style="background-color: rgba(197, 56, 56, 0.527)">{{$tran->state}}</td>@endif
						

						<td style="width: 270px;">

							@if(auth()->user()->has_access_to('update',$tran))
							<a href="{{route('admin.transaction.edit',$tran)}}">
								<span class="btn  btn-outline-success btn-sm font-1 mx-1">
									<span class="fas fa-wrench "></span> تحكم
								</span>
							</a>
							@endif
							@if(auth()->user()->has_access_to('delete',$tran))
							<form method="POST" action="{{route('admin.transaction.destroy',$tran)}}" class="d-inline-block">@csrf @method("DELETE")
								<button class="btn  btn-outline-danger btn-sm font-1 mx-1" onclick="var result = confirm('هل أنت متأكد من عملية الحذف ؟');if(result){}else{event.preventDefault()}">
									<span class="fas fa-trash "></span> حذف
								</button>
							</form>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-12 p-3">
			
		</div>
	</div>
</div>
@endsection
