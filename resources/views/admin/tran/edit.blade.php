@extends('layouts.admin')
@section('content')
<div class="col-12 p-3">
    <div class="col-12 col-lg-12 p-0 ">
        <form id="validate-form" class="row" enctype="multipart/form-data" method="POST" action="{{route('admin.transaction.update',$tran)}}">
            @csrf
            @method("PUT")
            <div class="col-12 col-lg-8 p-0 main-box">
                <div class="col-12 px-0">
                    <div class="col-12 px-3 py-3">
                        <span class="fas fa-info-circle"></span> تعديل المعامله
                    </div>
                    <div class="col-12 divider" style="min-height: 2px;"></div>
                </div>
                <div class="col-12 p-3 row">

                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            الحاله
                        </div>
                        <div class="col-12 pt-3">
                            <select class="form-control" name="state" required>
                                <option value selected disabled hidden> الحالة</option>

                                <option  selected >{{$tran->state}}</option>
                                <option  value="مقبوله و قيد العمل" >مقبوله و قيد العمل</option>
                                <option  value="تم الانجاز" >تم الانجاز</option>
                                <option  value="عمليه الدفع">عمليه الدفع</option>
                                <option  value="مرفوضه" >مرفوضه</option>

                            </select>
                        </div>

                    </div>
                    <div class="col-12">
                    </div>

                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            المعامله
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="tran_name" required maxlength="190" class="form-control" value="{{$tran->tran_name}}">
                        </div>
                    </div>


                    @if($tran->tran_name == 'كشف علامات')

                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم الطالب
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="name" required maxlength="190" class="form-control" value="{{$tran->user->name}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم الاب
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="father" required maxlength="190" class="form-control" value="{{$tran->father}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم الام
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="mother" required maxlength="190" class="form-control" value="{{$tran->mother}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            القسم
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="section" required maxlength="190" class="form-control" value="{{$tran->section}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           سنة التسجيل في الجامعه
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="reg_uni" required maxlength="190" class="form-control" value="{{$tran->reg_uni}}">
                        </div>
                    </div>


                    @endif
<!--################################################################################################-->

                    @if ($tran->tran_name == 'اعتراض عملي')
                        
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم الطالب
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="name" required maxlength="190" class="form-control" value="{{$tran->user->name}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            الرقم الجامعي
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="id_uni" required maxlength="190" class="form-control" value="{{$tran->user->id_uni}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                             طالب السنه
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="year" required maxlength="190" class="form-control" value="{{$tran->year}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            القسم
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="section" required maxlength="190" class="form-control" value="{{$tran->section}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           سنه المقرر
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="course_year" required maxlength="190" class="form-control" value="{{$tran->course_year}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           سنه المقرر
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="course_year" required maxlength="190" class="form-control" value="{{$tran->course_year}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           الفصل
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="chapter" required maxlength="190" class="form-control" value="{{$tran->chapter}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           المدرس
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="teach" required maxlength="190" class="form-control" value="{{$tran->teach}}">
                        </div>
                    </div>


                    @endif
<!--################################################################################################-->
                    @if ($tran->tran_name == 'وثيقه دوام')
                        
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم الطالب
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="name" required maxlength="190" class="form-control" value="{{$tran->user->name}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم الاب
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="father" required maxlength="190" class="form-control" value="{{$tran->father}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                             طالب السنه
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="year" required maxlength="190" class="form-control" value="{{$tran->year}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            القسم
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="section" required maxlength="190" class="form-control" value="{{$tran->section}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           الفصل
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="chapter" required maxlength="190" class="form-control" value="{{$tran->chapter}}">
                        </div>
                    </div>

                    @endif



                    @if ($auther->power == 'ADMIN')
                        
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            الموظف
                        </div>
                        <div class="col-12 pt-3">
                            <select class="form-control" name="auther_id" required>
                                <option value selected disabled hidden> اختر الموظف</option>
                                @if($tran->auther_id != null) 
                                <option value="{{$tran->auther_id}}" selected >{{$tran->auther->name}} </option>
                                @endif
                                @foreach($emp as $tran)
                                {{-- <option value="{{$tran->id}}" @if(old('auther_id')==$tran->id) selected @endif>{{$tran->name}}</option> --}}
                                <option   value="{{$tran->id}}" >{{$tran->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    @else
                        
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           الموظف
                        </div>
                        <select class="form-control" name="auther_id" required>
                            <option value selected disabled hidden>إختر الموظف</option>
                    
                            <option value="{{$auther->id}}"  selected >{{$auther->name}}</option>
                          
                        </select>
                    </div>


                    @endif





                        {{-- <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                الرقم الجامعي
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="id_uni" required maxlength="190" class="form-control" value="{{$tran->user->id_uni}}">
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                اسم الطالب
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="name" required maxlength="190" class="form-control" value="{{$tran->user->name}}">
                            </div>
                        </div>
                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                اسم الاب
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="father" required maxlength="190" class="form-control" value="{{$tran->father}}">
                            </div>
                        </div>

                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم الام
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="mother" required maxlength="190" class="form-control" value="{{$tran->mother}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            القسم
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="section" required maxlength="190" class="form-control" value="{{$tran->section}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           سنة التسجيل في الجامعه
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="reg_uni" required maxlength="190" class="form-control" value="{{$tran->reg_uni}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           سنه المقرر
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="course_year" required maxlength="190" class="form-control" value="{{$tran->course_year}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                            اسم المقرر
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="course_name" required maxlength="190" class="form-control" value="{{$tran->course_name}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           الفصل
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="chapter" required maxlength="190" class="form-control" value="{{$tran->chapter}}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 p-2">
                        <div class="col-12">
                           المدرس
                        </div>
                        <div class="col-12 pt-3">
                            <input type="text" name="teach" required maxlength="190" class="form-control" value="{{$tran->teach}}">
                        </div>
                    </div> --}}

                    
                </div>
                </div>
                </div>


                </div>
            </div>
            <div class="col-12 p-3">
                <button class="btn btn-success" id="submitEvaluation">حفظ</button>
            </div>
        </form>
    </div>
</div>
@endsection
